#!/usr/bin/env python
# -*- coding: utf8 -*-

print('Программа для шифрования строки методом "Скитала"')
s = str(input('Введите строку для шифрования: '))
step = int(input('Введите шаг шифрования: '))

# Добавляем к строке пробелы,
# пока размер не будет кратным шагу:
if (len(s) % step) != 0:
    for i in range(step - len(s) % step):
        s += ' '

print('Таблица для шифрования:')
for i in range(0, len(s), step):
    print(s[i:(i+step)])

print('Зашифрованная строка:')
i = 0
s_encrypted = ''
for x in range(len(s)):
    s_encrypted += s[(i % len(s) + (i // len(s)))]
    i += step
print(s_encrypted)
