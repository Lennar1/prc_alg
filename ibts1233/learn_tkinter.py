import tkinter

form = tkinter.Tk()

def hello(event):
    print("Кнопка пишет в командную строку")

btn = tkinter.Button(form,text="Кнопка",
                     width=30,height=5,
                     bg="white",fg="black")

btn.bind("<Button-1>", hello)
btn.place(x=10,y=50)

btn2 = tkinter.Button(form,text="Кнопка 2")
btn2.place(x=300,y=50)
btn3 = tkinter.Button(form,text="Кнопка 3")
btn3.place(x=10,y=300)
btn4 = tkinter.Button(form,text="Кнопка 4")
btn4.place(x=300,y=300)



form.mainloop()
