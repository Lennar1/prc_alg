#!/bin/python3

'''
Программа сортировки списка (массива) чисел.
'''

'''
В первой части задаём массив случайных чисел
с помощью цикла for
'''

from random import randint


list_of_numbers = []

for i in range(0, 100):
    list_of_numbers.append(randint(-100,100))

print(list_of_numbers)
print(sum(list_of_numbers))
