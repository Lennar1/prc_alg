# -*- coding: utf8 -*-

'''
Программа запрашивает строку у пользователя
и выводит ее на экран до тех пор, пока пользователь
не введет "пока" или "до свидания"
'''

import random

bye_words = ['пока',
             'до свидания',
             'выход',
             'закройся'
             ]

hello_words = ['привет',
               'хай',
               'здравствуй'
               ]

how_words = ['как дела',
             'как жизнь',
             'как ты'
             ]

self_hello_words = ['и тебе не болеть',
                    'привет',
                    'здравствуйте'
                    ]

x = input('Введите строку: ')

while x not in bye_words:
    if x in hello_words:
        l = len(self_hello_words)
        i = random.randint(0,l-1)
        print(self_hello_words[i])
    elif x in how_words:
        print('Нормально, а ты как?')
    else:
        print('Моя твоя нипанимай')

    x = input('Введите строку: ')

print('До свидания!')
