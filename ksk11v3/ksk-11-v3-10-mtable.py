#!/usr/bin/env python

def question(x1, x2):
    y = x1 * x2
    string_question = '%d * %d = ' % (x1, x2)
    answer = int(input(string_question))
    if answer == y:
        print('Правильно!')
    else:
        print('Неправильно!')


def is_continue():
    q = input('\nНажмите "Y" для продолжения: ')
    return q in ['y', 'Y']
   
 
if __name__ == '__main__':
    print('Начало программы\n')
    while is_continue():
        j = int(input('Введите число для изучения: '))
        for i in range(1, 11):
            question(i, j)
        
    print('До свидания!')
