#!/usr/bin/env python
print('Вычисление общей потребляемой мощности кабинета')

# Мощности устройств
lamp_power = 40
pc_power = 400

lamp_count = int(input('Введите количество ламп: '))
pc_count = int(input('Введите количество ПК: '))
all_lamp_power = lamp_count * lamp_power
all_pc_power = pc_count * pc_power
all_power = all_lamp_power + all_pc_power
print('\tМощность ламп:', all_lamp_power, 'Вт\n',
      '\tМощность всех ПК:', all_pc_power, 'Вт\n',
      '\tМощность общая:', all_power, 'Вт')
